package com.testapi;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.ParallelFlux;

import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

@RestController
public class Streamcontroller {

    Random rand = new Random();
    int count = 0;
     @GetMapping("/stream")
    public Mono<Boolean> returntruefalse(){
         return Mono.just(rand.nextInt())
                 .map((s) -> {
                     if(s%2==0) return true;
                     else return false;
                 }
                     );
     }
    @GetMapping(value = "/streamflow",produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
    public Flux<Boolean> returnstream(){
        return Mono.just(count).repeat(5)
                .map((s) -> {
                            count++;
                            if(s%2==0) return true;
                            else return false;
                        }
                )
                .delayElements(Duration.ofSeconds(2));

    }

    //sequential call
    @GetMapping("/callapiseq")
    public Flux<Boolean>  callseq() {
        Mono<Boolean> call1 = returntruefalse();
        Mono<Boolean> call2 = returntruefalse();
        Mono<Boolean> call3 = returntruefalse();
        Mono<Boolean> call4 = returntruefalse();
        return Flux.merge(call1, call2, call3, call4).log();
    }

    @GetMapping("/callapiseq2")
    public Mono<List<Boolean>> callseq2(){
         return returntruefalse()
                 .flatMap(s-> { return returntruefalse()
                         .flatMap(r-> {
                             return returntruefalse()
                                     .flatMap(t->{
//                                         List<Boolean> f = new ArrayList<Boolean>();
//                                         f.add(s);f.add(r);
                                         return Mono.just(Arrays.asList(s,r));
                                     });
                         });

                 });
    }

    //parallel
    @GetMapping("/callapipar/{t}")
    public ParallelFlux<Object> callpar(@PathVariable Integer t) {
         ParallelFlux<Object> f = Flux
                                    .range(0,1)
                            .parallel()
                            .log()
                            .flatMap( s-> {
                                Mono<Boolean> call1 = returntruefalse();
                                Mono<Boolean> call2 = returntruefalse();
                                Mono<Boolean> call3 = returntruefalse();
                                Mono<Boolean> call4 = returntruefalse();
                                return Flux.merge(call1, call2, call3, call4);
                                /*return returntruefalse().log();*/
                                    }
                            );
         return f;

    }


/*    @GetMapping("/callapipar2/{t}")
    public Flux<Boolean>  callpar2(@PathVariable Integer t) {
        Flux<Boolean> f = Mono.just(t)
                          .flatMap(s->{
                              returntruefalse().
                                  }

                          )
        return f;

    }*/

    int t = 0, f = 0;
    @GetMapping("/callapi/{no}")
    Flux<Integer> callstream(@PathVariable int no) {
        return returntruefalse().repeat(no)
                .collectList()
                .flatMapMany(s -> {
                    for (Boolean b : s) {
                        if (b) ++t;
                        else ++f;

                    }
                    return Flux.just(t, f);
                });
    }

}

